import {Mongo} from 'meteor/mongo';

export const DeputyRequestsCollection = new Mongo.Collection('deputyRequests');

if(Meteor.isServer) {
  Meteor.publish('DeputyRequests', () => {
    return DeputyRequestsCollection.find({}, {sort: {createdAt: -1}, limit: 20});
  });

  Meteor.publish('KzDeputyRequests', () => {
    return DeputyRequestsCollection.find({'info.kz': {$ne: ""}}, {sort: {createdAt: -1}, limit: 20});
  });

  Meteor.publish('RuDeputyRequests', () => {
    return DeputyRequestsCollection.find({'info.ru': {$ne: ""}}, {sort: {createdAt: -1}, limit: 20});
  });

  Meteor.publish('AllDeputyRequests', () => {
    return DeputyRequestsCollection.find({}, {sort: {createdAt: -1}});
  });

  Meteor.methods({
    'deputyRequests.add'(data) {
      data.createdBy = Meteor.userId;

      DeputyRequestsCollection.insert(data);
    },
    'deputyRequests.remove'(_id) {
      DeputyRequestsCollection.remove({_id})
    },
    'deputyRequests.edit'(_id, data) {
      DeputyRequestsCollection.update({_id}, {$set: data});
    }
  });
}
