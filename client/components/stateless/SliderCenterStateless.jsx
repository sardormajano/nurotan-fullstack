import React, {Component} from 'react';
import {addScript, keepRunningTillLoaded} from '../../lib/coreLib.js';
import { Link } from 'react-router'

export default class SliderCenterStateless extends Component {
  componentDidMount() {
    $.getScript('/custom/js/slider/owl.carousel.min.js', () => {
      $('#slider-center .item.premount').removeClass('premount');

      $(this.sc).owlCarousel({
         loop: true,
         items: 1,
         margin: 10,
         nav: true,
         pagination: true,
         navText: ["<", ">"],
         responsive: {
             0: {
                 items: 2
             },
             960: {
                 items: 3
             }
         }
     });
    });
  }

  render() {
    const {projects} = this.props,
          projectsJSX = projects.map((project) => {
            return (
              <div key={project._id} className="item premount">
                <Link to={"/single-project?id="+project._id}><img src={project.photo + "-/scale_crop/211x164/"} alt={project.title} /></Link>
              </div>
            );
          });
    return (
      <div id="slider-center" className="project-custom">
        <h2><Link className="articles-theme-button" to="/projects"><i>{this.props.mpStrings.projects}</i></Link></h2>
        <div ref={(sc) => {this.sc = sc;}} className="columns-project">
          {projectsJSX}
        </div>
      </div>
    );
  }
}
