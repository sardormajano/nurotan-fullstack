import React, {Component} from 'react';

export default class Leader extends Component {
  render() {
    const {setActiveSection, activeSectionNumber, mpStrings} = this.props;

    return(
      <div>
        {this.props.header}
        {/* Main*/}
        <div id="main">
          <div className="theme-bg-green center">
            <h1>{mpStrings.partyLeader}  {mpStrings.nazarbaev}</h1>
          </div>
          <div ref={(tabs) => {this.tabs = tabs}} className="tabs leader-page center" data-tabs>
            <ul id="tabs-click" className="tab-article">
              <li className={activeSectionNumber === 0 ? "active" : ""}><a onClick={setActiveSection.bind(this)} data-section="0" href="#">{mpStrings.partyLeader}</a></li>
              <li className={activeSectionNumber === 1 ? "active" : ""}><a onClick={setActiveSection.bind(this)} data-section="1" href="#">{mpStrings.performances}</a></li>
              <li className={activeSectionNumber === 2 ? "active" : ""}><a onClick={setActiveSection.bind(this)} data-section="2" href="#">{mpStrings.messages} </a></li>
              <li className={activeSectionNumber === 3 ? "active" : ""}><a onClick={setActiveSection.bind(this)} data-section="3" href="#">{mpStrings.quotations}</a></li>
              <li className={activeSectionNumber === 4 ? "active" : ""}><a onClick={setActiveSection.bind(this)} data-section="4" href="#">{mpStrings.photos}</a></li>
            </ul>
            {/*Leader tabs content*/}
            {this.props.activeSection}
          </div>
          {this.props.sliderBlock}
        </div>
        {/* End Main */}
        {this.props.footer}
        {/*Start increase and decrease*/}
        <div id="font-block-fixed">
          <button id="increase">+</button><br />
          <button id="decrease">-</button>
        </div>
        {/*End increase and decrease*/}
      </div>
    );
  }
}
