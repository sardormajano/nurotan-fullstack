import React, {Component} from 'react';

export default class Footer extends Component {
  render() {
    return (
      <footer className="main-footer">
        <div className="container">
          <div className="pull-right hidden-xs">
            <b>Версия</b> 1.0
          </div>
          <strong>© 2016 <a href="#">Сардорбек Мамажанов</a>.</strong> Все права защищены.
        </div>
        {/* /.container */}
      </footer>
    );
  }
}
