import React, {Component} from 'react';

import Helmet from 'react-helmet';

export default class SearchContainerHelmet extends Component {
  render() {
    return (
      <Helmet
        link={[
        ]}
        script={[
          {type: 'text/javascript', src: '/custom/js/vendor/jquery-1.12.0.min.js'},
          {type: 'text/javascript', src: '/custom/js/plugins.js'},
        ]} />
    );
  }
}
