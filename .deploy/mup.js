module.exports = {
  servers: {
    one: {
      host: '88.204.198.140',
      username: 'sardor',
      // pem:
      password: 'MBc63AMG',
      opts: {
        port: 8922
      }
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    name: 'nurotan-fullstack',
    path: '.',
    dockerImage: 'abernix/meteord:base',
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      ROOT_URL: 'http://localhost:3000/',
      //PORT: '',
      MONGO_URL: 'http://localhost:3001/'
    },

    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 120
  },

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};
