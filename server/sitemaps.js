import {NewsCollection} from '../api/News.js';

const newsArray = NewsCollection.find({}, {sort: {createdAt: -1}}).fetch();

const pages = [];

pages.push({
    page: 'http://nurotan.kz',
    lastmod: newsArray[0].createdAt
  });

pages.push({
    page: 'http://nurotan.kz/news?tab=0',
    lastmod: newsArray[0].createdAt
  });

for(let i = 0, len = newsArray.length; i < len; i++) {
  pages.push({
      page: 'http://nurotan.kz/single-news?id='+newsArray[i]._id+'&amp;lang=ru',
      lastmod: newsArray[i].createdAt
    });

  pages.push({
      page: 'http://nurotan.kz/single-news?id='+newsArray[i]._id+'&amp;lang=kz',
      lastmod: newsArray[i].createdAt
    });
}

sitemaps.add('/sitemap.xml', pages);
